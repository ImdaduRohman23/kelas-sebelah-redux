import React from 'react'

const NavbarImage = ({data}) => {
    return (
        <div>
            <img src={data.avatar} alt="" />
        </div>
    )
}

export default NavbarImage
