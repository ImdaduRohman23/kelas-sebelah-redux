import React from 'react'

const NavbarName = ({data}) => {
    return (
        <div>
            <h1>{data.nama}</h1>
        </div>
    )
}

export default NavbarName
