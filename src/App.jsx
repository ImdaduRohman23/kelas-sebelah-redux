import './App.css'
import Card from './component/Card'
import Navbar from './component/Navbar'

function App() {

  return (
    <div>
      <Navbar />
      <div className="App">
        <Card />
      </div>
    </div>
  )
}

export default App
